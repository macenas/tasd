package client;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

public class CRH {
	private Socket socket;
	private static CRH instance;
	
	public static CRH getInstance() {
		
		if (instance == null)
			instance = new CRH();
			
		return instance;
	}
	

	public byte[] send(String address, int port, byte[] serializedRequest) {
		byte[] serializedResponse = new byte[50000];
		
		try {
			
			if ((this.socket == null) || (this.socket.isClosed()))
				this.socket = new Socket(address, port);
			
			DataOutputStream output = new DataOutputStream(this.socket.getOutputStream());
			output.write(serializedRequest);
			output.flush();
			DataInputStream input = new DataInputStream(this.socket.getInputStream());
			input.read(serializedResponse);
			output.close();
			this.socket.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return serializedResponse;
	}
	
}
