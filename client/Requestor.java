package client;

import lookup.CallbackThread;

import common.Marshaller;
import common.Request;
import common.Response;
import common.interfaces.CallbackObject;

public class Requestor {
	private static Requestor instance;
	
	public static Requestor getInstance() {
		
		if (instance == null)
			instance = new Requestor();
			
		return instance;
	}
	
	public Response sendInvocation(Request request) {
		String address = request.getAOR().getAddress();
		int port = request.getAOR().getPort();
		byte[] serializedResponse = CRH.getInstance().send(address, port, 
				Marshaller.serialize(request));
		return ((Response) Marshaller.deserialize(serializedResponse));
	}
	
	public void sendInvocation(Request request, CallbackObject callbackObject) {
		CallbackThread thread = new CallbackThread(request, callbackObject);
		thread.start();
	}
	
}
