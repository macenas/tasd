package lookup;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import server.Invoker;
import server.SRH;

import common.ListeningThread;
import common.interfaces.LookupListener;

public class LookupServer {
	private HashMap<String, AOR> objectMap = new HashMap<String, AOR>(10, 10); 
	private static LookupServer instance;
	private ArrayList<LookupListener> listenerList;
	
	public static LookupServer getInstance() {
		
		if (instance == null)
			instance = new LookupServer();
			
		return instance;
	}
	
	public LookupServer() {
		Invoker.getInstance().register(this);
		this.listenerList = new ArrayList<LookupListener>(1);
	}
	
	public void addListener(LookupListener listener) {
		this.listenerList.add(listener);
	}
	
	public String[] list() {
		String[] objectList = new String[this.objectMap.size()];
		Iterator<String> objectMapIterator = this.objectMap.keySet().iterator();
		int i = 0;
		
		while (objectMapIterator.hasNext()) {
			objectList[i] = objectMapIterator.next();
			i++;
		}
		
		return objectList;
	}
	
	public AOR getAOR(String objectName) {
		return this.objectMap.get(objectName);
	}
	
	public String register(String objectName, AOR aor) {
		String result = "Registrado.";
		
		if (this.objectMap.get(objectName) != null)
			result = "Já existe um objeto registrado com o nome especificado.";
		else {
			objectMap.put(objectName, aor);
			Iterator<LookupListener> iterator = this.listenerList.iterator(); 
			
			while (iterator.hasNext())
				iterator.next().onRegister(objectName, aor);
			
		}
		
		return result;
	}
	
	// FIXME: Anybody can unregister any object. Have to fix it. 
	public String unregister(String objectName) {
		String result = "Desregistrado.";
		
		if (this.objectMap.get(objectName) == null)
			result = "Não existe objeto registrado com o nome especificado.";
		else {
			this.objectMap.remove(objectName);
			Iterator<LookupListener> iterator = this.listenerList.iterator(); 
			
			while (iterator.hasNext())
				iterator.next().onUnregister(objectName);
			
		}
		
		return result;
	}
	
	public void start(int port) {
		SRH srh = new SRH(port);
		ListeningThread thread = new ListeningThread(srh);
		thread.start();
	}
	
}