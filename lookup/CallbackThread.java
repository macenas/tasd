package lookup;

import client.CRH;

import common.Marshaller;
import common.Request;
import common.Response;
import common.interfaces.CallbackObject;

public class CallbackThread extends Thread {
	private Request request;
	private CallbackObject callbackObject;
	
	public CallbackThread(Request request, CallbackObject callbackObject) {
		this.request = request;
		this.callbackObject = callbackObject;
	}

	@Override
	public void run() {
		String address = this.request.getAOR().getAddress();
		int port = this.request.getAOR().getPort();
		byte[] serializedResponse = CRH.getInstance().send(address, port, 
				Marshaller.serialize(request));
		Response response = 
				((Response) Marshaller.deserialize(serializedResponse));
		this.callbackObject.run(response.getResult());
	}
	
}
