package lookup;

import java.io.Serializable;

public class AOR implements Serializable {
	private static final long serialVersionUID = 7305621626751931388L;
	private int id;
	private String address;
	private int port;
	
	public AOR(String address, int port, int id) {
		this.address = address;
		this.port = port;
		this.id = id;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}
	
}
