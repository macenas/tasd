package lookup;

import java.net.InetAddress;
import java.net.UnknownHostException;

import server.Invoker;
import client.Requestor;

import common.Request;
import common.Response;

public class LookupClient {
	private String lookupAddress;
	private int lookupPort;
	private static LookupClient instance;
	
	public static LookupClient getInstance() {
		
		if (instance == null)
			instance = new LookupClient();
			
		return instance;
	}
	
	public void setLookupAddress(String lookupAddress) {
		this.lookupAddress = lookupAddress;
	}

	public void setLookupPort(int lookupPort) {
		this.lookupPort = lookupPort;
	}
	
	public AOR getAOR(String objectName) {
		AOR lookupAOR = new AOR(this.lookupAddress, this.lookupPort, 0);
		Request request = new Request(lookupAOR, "getAOR", 
				(new Object[] {objectName}));
		Response response = Requestor.getInstance().sendInvocation(request);
		return ((AOR) response.getResult());
	}
	
	public String register(String objectName, int objectPort, Object object) {
		int objectId = Invoker.getInstance().register(object);
		String localAddress = null;
		
		try {
			localAddress = InetAddress.getLocalHost().getHostAddress();
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			System.out.println("Impossível resolver o endereço local.");
			e.printStackTrace();
		}
		
		AOR objectAOR = new AOR(localAddress, objectPort, objectId);
		Object[] arguments = new Object[] {objectName, objectAOR};
		AOR lookupAOR = new AOR(this.lookupAddress, this.lookupPort, 0);
		Request request = new Request(lookupAOR, "register", arguments);
		Response response = Requestor.getInstance().sendInvocation(request);
		
		if (!response.getResult().equals("Registrado."))
			Invoker.getInstance().unregister(objectId);
		
		return ((String) response.getResult());
	}
	
	// TODO: Implement method body.
	public String unregister(String objectName) {
		return null;
	}
	
	public String[] list() {
		AOR lookupAOR = new AOR(this.lookupAddress, this.lookupPort, 0);
		Request request = new Request(lookupAOR, "list", (new Object[] {}));
		Response response = Requestor.getInstance().sendInvocation(request);
		return ((String[]) response.getResult());
	}
	
}
