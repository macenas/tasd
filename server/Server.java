package server;

import common.ListeningThread;

public class Server {
	private static Server instance;
	
	public static Server getInstance() {
		
		if (instance == null)
			instance = new Server();
			
		return instance;
	}	
	
	public int start(int port) {
		SRH srh = new SRH(port);
		ListeningThread thread = new ListeningThread(srh);
		thread.start();
		return srh.getPort();
	}
	
}