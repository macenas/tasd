package server;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class SRH {
	private ServerSocket serverSocket;
	private Socket socket;
	private DataInputStream input;
	private DataOutputStream output;
	
	public SRH(int port) {
		
		try {
			serverSocket = new ServerSocket(port);
		} catch (IOException e) {
			//TODO: Auto-generated catch block
			System.out.println(
					"IO error while listening to request from client.");
			e.printStackTrace();
		}
		
	}
	
	public void listen() {

		try {
			this.socket = serverSocket.accept();
			this.input = new DataInputStream(this.socket.getInputStream());
			byte[] serializedRequest = new byte[50000];
			this.input.read(serializedRequest);
			byte[] serializedResponse = 
					Invoker.getInstance().invoke(serializedRequest);
			this.output = 
					new DataOutputStream(this.socket.getOutputStream());
			this.output.write(serializedResponse);
		} catch (IOException e) {
			//TODO: Auto-generated catch block
			System.out.println(
					"IO error while listening to request from client.");
			e.printStackTrace();
		}
		
	}
	
	// Used to get actual port when server is started with random port number.
	public int getPort() {
		return this.serverSocket.getLocalPort();
	}
	
}
