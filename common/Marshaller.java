package common;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class Marshaller {

	public static byte[] serialize(Object object) {
		ByteArrayOutputStream byteArrayStream = new ByteArrayOutputStream();
		ObjectOutputStream objectStream = null;
		byte[] serializedObject = null;
		
		try {
			objectStream = new ObjectOutputStream(byteArrayStream);   
			objectStream.writeObject(object);
			serializedObject = byteArrayStream.toByteArray();
			objectStream.close();
			byteArrayStream.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return serializedObject;
	}
	
	public static Object deserialize(byte[] serializedObject) {
		ByteArrayInputStream byteArrayStream = new ByteArrayInputStream(serializedObject);
		ObjectInputStream objectStream = null;
		Object object = null;
		
		try {
			objectStream = new ObjectInputStream(byteArrayStream);
			object = objectStream.readObject(); 
			byteArrayStream.close();
			objectStream.close();
		} catch (IOException | ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return object;
	}
	
}
