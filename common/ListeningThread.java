package common;

import server.SRH;

public class ListeningThread extends Thread {
	private SRH srh;
	
	public ListeningThread(SRH srh) {
		this.srh = srh;
	}
	
	public void run() {
		
		while (true)
			this.srh.listen();
		
	}
	
}