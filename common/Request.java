package common;

import java.io.Serializable;

import lookup.AOR;

public class Request implements Serializable {
	private static final long serialVersionUID = -2745866110091550108L;
	private AOR aor;
	private String operation;
	private Object[] arguments;
	
	public Request(AOR aor, String operation, Object[] arguments) {
		this.setObject(aor);
		this.setOperation(operation);
		this.setArguments(arguments);
	}

	public AOR getAOR() {
		return this.aor;
	}

	public void setObject(AOR aor) {
		this.aor = aor;
	}

	public String getOperation() {
		return this.operation;
	}

	public void setOperation(String operation) {
		this.operation = operation;
	}

	public Object[] getArguments() {
		return this.arguments;
	}

	public void setArguments(Object[] arguments) {
		this.arguments = arguments;
	}

}