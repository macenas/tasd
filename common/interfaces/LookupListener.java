package common.interfaces;

import lookup.AOR;

public interface LookupListener {
	public void onRegister(String objectName, AOR aor);
	public void onUnregister(String objectName);
}
