package common.interfaces;

public interface CallbackObject {
	public void run(Object result);
}
